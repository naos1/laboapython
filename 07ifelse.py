#
# 07ifelse.py
#

for i in range(100):
    
    j = i % 2
    
    if j == 0:
       print('even number', i)
       
    else:
       print('odd number ', i)
