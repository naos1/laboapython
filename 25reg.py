#
# 25reg.py
# Linear regression between Tahiti SLP and Darwin SLP (for each month)
import numpy as np
from scipy import stats

# text data
data_1 = np.loadtxt('data/slpdarwin-ym.dat', usecols=range(1, 13))
data_2 = np.loadtxt('data/slptahiti-ym.dat', usecols=range(1, 13))
itime, id = data_1.shape

a1 = np.zeros((id))
a0 = np.zeros((id))

for m in range(id):
    slope, intercept, _, _, _ = \
    stats.linregress(data_1[:, m], data_2[:, m])
    a1[m], a0[m] = slope, intercept

print('A1', a1)
print('A0', a0)
