#
# 09sample.py
#

print('input a positive integer')
i = int(input('>>'))

if i <= 1 or i >=60000:
   print('invalid input')
   exit()

flag = 0

for j in range(2, i):
    a = i % j
    if a == 0:
       flag = 1

if flag == 1:
   print(i, 'is NOT a prime')
else:
   print(i, 'is a prime')
