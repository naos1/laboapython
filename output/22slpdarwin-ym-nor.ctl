dset ^22slpdarwin-ym-nor.bin
undef -999
options yrev big_endian
xdef   1 linear   0 2.5
ydef   1 linear -90 2.5
tdef 145 linear 00Z01jan1866 1yr
zdef   1 levels 1000
vars  12
jan   1 0 some variable
feb   1 0 some variable
mar   1 0 some variable
apr   1 0 some variable
may   1 0 some variable
jun   1 0 some variable
jul   1 0 some variable
aug   1 0 some variable
sep   1 0 some variable
oct   1 0 some variable
nov   1 0 some variable
dec   1 0 some variable
endvars
