dset ^20uwndavevar9201.bin
undef -999
options yrev big_endian
xdef 144 linear   0 2.5
ydef  73 linear -90 2.5
tdef   1 linear 00Z01jan1992 1mo
zdef   1 levels 1000
vars   2
uave   1 0 some variable
uvar   1 0 some variable
endvars
