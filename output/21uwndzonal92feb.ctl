dset ^21uwndzonal92feb.bin
undef -999
options yrev big_endian
xdef   1 linear   0 2.5
ydef  73 linear -90 2.5
tdef   1 linear 00Z01feb1992 1mo
zdef   1 levels 1000
vars   1
u      1 0 some variable
endvars
