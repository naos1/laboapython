#
# 18clim-2d.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/slp.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

clim = np.zeros((12, ilat, ilon))

# sum up
for imon in range(12):
     for it in range(imon, itime, 12):
          clim[imon, :, :] = clim[imon, :, :] + data_in[it, :, :]

# divide by 10
clim = clim / 10

out = open('output/18slpclim9201-2d.bin', 'wb')
data = np.array(clim, dtype='>f4')
out.write(data)
