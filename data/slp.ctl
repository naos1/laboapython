dset ^slp.bin
undef -999
options yrev big_endian
xdef 144 linear   0 2.5
ydef  73 linear -90 2.5
tdef  120 linear 00Z01jan1992 1mo
zdef 1 levels 1000
vars 1
slp  1 0 some variable
endvars
