#
# 10-2types.py
#

a = 5
b = 5.0
c = '5'
d = True

e = float(a)
f = int(b)
g = str(a)

h = 6 - 3
i = 6 / 3

ls=[a,b,c,d,e,f,g,h,i]
nm=['a','b','c','d','e','f','g','h','i']
for n, s in zip(nm, ls):
    print( n+' =', s, type(s) )
