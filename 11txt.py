#
# 11txt.py
#
import numpy as np

data = np.loadtxt('data/tavegl.dat')

data2 = data[0::2, :]

data3 = data[1::2, :]

np.savetxt('output/11tmp.dat', data2, fmt=['%d']+13*['%.2f'])
np.savetxt('output/11obs.dat', data3,
           delimiter=',', fmt='%d')
