#
# 18clim-func.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/slp.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

clim = np.zeros((12, ilat, ilon))

for imon in range(12):
     clim[imon, :, :] = np.mean(data_in[imon::12, :, :], axis=0)

out = open('output/18slpclim9201-func.bin', 'wb')
data = np.array(clim, dtype='>f4')
out.write(data)
