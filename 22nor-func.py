#
# 22nor-func.py
#
import numpy as np
from scipy import stats

data = np.loadtxt('data/slpdarwin-ym.dat', usecols=range(1,13))

nor = stats.zscore(data, axis=0)

np.savetxt(f'output/22slpdarwin-ym-nor-func.dat', nor, fmt='%.2f')
out2 = open(f'output/22slpdarwin-ym-nor-func.bin', 'wb')
dout = np.array(nor, dtype='>f4')
out2.write(dout)

# check standardization
print('=== check standardization ===')
c_ave = np.mean(nor, axis=0)
c_std = np.std(nor, axis=0)
print('ave', np.round(c_ave, 2))
print('std', c_std)
