#
# 20avevar-func.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in =  np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

# ave, var, std
ave = np.mean(data_in, axis=0)
var = np.var(data_in, axis=0)
std = np.std(data_in, axis=0)

out = open('output/20uwndavevar9201.bin', 'wb')

data_out1 = np.array(ave, dtype='>f4')
data_out2 = np.array(var, dtype='>f4')
out.write(data_out1)
out.write(data_out2)
