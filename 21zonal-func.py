#
# 21zonal-func.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

zave = np.mean(data_in[1, :, :], axis=1) # 1992 feb
