#
# 02simplemath.py
#

pi = 3.141592  #pi
r = 6378136    #radius of earth

rkm = r / 1000
volume = 4 / 3 * pi * rkm**3
volume = volume / 1000000

print('volume is', volume)
