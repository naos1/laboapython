#
# 04loop.py
#

for i in range(1, 10, 2):
    print('counting up ...', i)

for i in range(9, 0, -2):
    print('counting down ...', i)
