#
# 16writebin.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/slp.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

data = data_in[7::12]

out = open('output/16slp9208.bin', 'wb')
data = np.array(data, dtype='>f4')
out.write(data)
