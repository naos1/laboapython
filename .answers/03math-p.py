#
# 03math.py
#
import math

deg = 30
pi = 3.1415
rad = deg * pi / 180.

print('sin is', math.sin(rad))
print('cos is', math.cos(rad))

rad2 = math.radians(deg)

print('sin is', math.sin(rad2))
print('cos is', math.cos(rad2))

print('tan is', math.sin(rad2) / math.cos(rad2))
print('tan is', math.tan(rad2))
