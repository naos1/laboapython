#
# 19anomaly-func-1.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

anom = data_in[71, :, :] - data_in[59, :, :] # 1997dec - 1996dec

out = open('output/19uwndanom97-96dec.bin', 'wb')
data_out = np.array(anom, dtype='>f4')
out.write(data_out)
