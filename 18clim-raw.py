#
# 18clim-raw.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/slp.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

clim = np.zeros((12, ilat, ilon))

# sum up
for imon in range(12):
     for it in range(imon, itime, 12):
          for i in range(ilat):
               for j in range(ilon):
                    clim[imon, i, j] = clim[imon, i, j] + data_in[it, i, j]
                    
# divide by 10
for imon in range(12):
     for i in range(ilat):
          for j in range(ilon):
               clim[imon, i, j] = clim[imon, i, j] / 10

out = open('output/18slpclim9201-raw.bin', 'wb')
data = np.array(clim, dtype='>f4')
out.write(data)
