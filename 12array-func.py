#
# 12array-func.py
#
import numpy as np

year = np.loadtxt('output/11tmp.dat', usecols=(0))
data = np.loadtxt('output/11tmp.dat', usecols=range(1,13))

mon = 8
i = mon - 1

sy = int(*np.where(year==1953))
ey = int(*np.where(year==2002))

data2 = data[sy:ey+1, :]
ave = np.mean(data2, axis=0)

print('output')
print(np.round(ave[i], decimals=2))

# output to file
np.savetxt('output/12tave-f.dat', [ave[i]], fmt='%.2f',
           header=f'1953-2002 mean month={mon}')
