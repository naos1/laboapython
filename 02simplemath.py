#
# 02simplemath.py
#

pi = 3.141592  #pi
r = 6378136    #radius of earth

rkm = r / 1000
area = 4 * pi * rkm**2
area = area / 1000000

print('area is', area)
