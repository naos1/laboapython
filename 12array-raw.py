#
# 12array-raw.py
#
import numpy as np

year = np.loadtxt('output/11tmp.dat', usecols=(0))
data = np.loadtxt('output/11tmp.dat', usecols=range(1,13))

mon = 8
i = mon - 1

ave = 0
for j in range(97, 147):
     ave = ave + data[j, i]
     print(j+1856, ave, data[j, i])
ave = ave / 50

print('output')
print(np.round(ave, decimals=2))

# output to file
np.savetxt('output/12tave.dat', [ave], fmt='%.2f',
           header=f'1953-2002 mean month={mon}')
