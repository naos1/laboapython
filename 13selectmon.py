#
# 13selectmon.py
#
import numpy as np

mon = 8
data = np.loadtxt('output/11tmp.dat', usecols=(0, mon))

# output to file
np.savetxt(f'output/13tave-{mon}.dat', data, fmt='%.2f',
           header=f'1953-2002 mean month={mon}')
