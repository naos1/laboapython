#
# 23index.py
# Southern Oscillation Index (SOI)
import numpy as np
from scipy import stats

itime, id = 145, 12

f = open('output/22slpdarwin-ym-nor.bin','rb')
slp_d = np.fromfile(f, dtype='>f4').reshape((itime, id))
f = open('output/22slptahiti-ym-nor.bin','rb')
slp_t = np.fromfile(f, dtype='>f4').reshape((itime, id))

soi = stats.zscore(slp_t - slp_d, axis=0)

o = open('output/23soi-ym-nor.bin', 'wb')
data_out = np.array(soi, dtype='>f4')
o.write(data_out) 
