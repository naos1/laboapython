#
# 20avevar-raw.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

ave = np.mean(data_in, axis=0)

var = np.zeros((ilat, ilon))

for it in range(0, itime):
     var = var + (data_in[it] - ave)**2

var = var / (itime - 1)

std = np.sqrt(var)

for i in range(0, ilat):
     for j in range(0, ilon):
          lon = 2.5 * j
          lat = 90 - 2.5 * i
          print(lon, lat, var[i, j], std[i, j])
