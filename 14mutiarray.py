#
# 14multiarray.py
#
import numpy as np

data = np.loadtxt('data/bitmap.dat', dtype='int')

print(data)

data = data.reshape((16, 16))
data = np.where(data == 1, '@', '.')

print(data)

np.savetxt('output/bitmap-1.dat', data, fmt='%s')
