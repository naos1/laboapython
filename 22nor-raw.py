#
# 22nor-raw.py
#
import numpy as np

name_in = 'slpdarwin-ym'
name_out = '22slpdarwin-ym-nor'

data = np.loadtxt(f'data/{name_in}.dat', usecols=range(1,13))

itime, id = data.shape

ave = np.mean(data, axis=0)
std = np.std(data, axis=0)

nor = np.zeros((itime, id))
for i in range(id):
    nor[:, i] = (data[:, i] - ave[i]) / std[i]

np.savetxt(f'output/{name_out}.dat', nor, fmt='%.2f')
out2 = open(f'output/{name_out}.bin', 'wb')
dout = np.array(nor, dtype='>f4')
out2.write(dout)

print('ave', ave)
print('std', std)

print('=== check standardization ===')
c_ave = np.mean(nor, axis=0)
c_std = np.std(nor, axis=0)
print('ave', np.round(c_ave, 2))
print('std', c_std)
