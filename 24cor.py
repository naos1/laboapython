#
# 24cor.py
# Correlation between Tahiti SLP and Darwin SLP (for each month)
import numpy as np

data_1 = np.loadtxt('data/slpdarwin-ym.dat', usecols=range(1,13))
data_2 = np.loadtxt('data/slptahiti-ym.dat', usecols=range(1,13))
itime, id = data_1.shape

# demo (jan)
cor_jan = np.corrcoef(data_1[:, 0], data_2[:, 0])
print(cor_jan)

cor = np.zeros((id))
for m in range(id):
    cor[m] = np.corrcoef(data_1[:, m], data_2[:, m])[0, 1]
print(cor)

np.savetxt('output/24cor-dar-tah.dat', cor, fmt='%.2f')
