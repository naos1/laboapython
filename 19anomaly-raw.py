#
# 19anomaly-raw.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

data1 = data_in[71, :, :]  # 1997dec
data2 = data_in[59, :, :]  # 1996dec

for i in range(ilat):
     for j in range(ilon):
          lon = 2.5 * j
          lat = 90 - 2.5 * i
          anom = data1[i, j] - data2[i, j]
          print(lon, lat, anom)
