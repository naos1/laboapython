#
# 24cor-raw.py
# Correlation between Tahiti SLP and Darwin SLP (for each month)
import numpy as np

itime = 145
id = 12

daved = np.zeros(id)
dvard = np.zeros(id)
dstdd = np.zeros(id)
dnord = np.zeros((itime, id))
davet = np.zeros(id)
dvart = np.zeros(id)
dstdt = np.zeros(id)
dnort = np.zeros((itime, id))
dcov = np.zeros(id)
dcor = np.zeros(id)

# text data
data_1 = np.loadtxt('data/slpdarwin-ym.dat').reshape(itime, 13)
data_2 = np.loadtxt('data/slptahiti-ym.dat').reshape(itime, 13)
data_3 = open('output/24cor-dar-tah-raw.dat', 'w')

# binary data
#f = open('data/slpdarwin-ym-std.bin','rb')
#data_1 = np.fromfile(f, dtype='>f4').reshape(itime, id)
#f.close()
#f = open('data/slptahiti-ym-std.bin','rb')
#data_2 = np.fromfile(f, dtype='>f4').reshape(itime, id)
#f.close()

iyear  = data_1[:, 0]
data_d = data_1[:, 1:13]
data_t = data_2[:, 1:13]

# ave
print('ave')

for it in range(0, itime):
    print(iyear[it], data_t[it, :])
    dslpd = data_d[it, :]
    dslpt = data_t[it, :]
    for i in range(0, id):
        daved[i] = daved[i] + dslpd[i]
        davet[i] = davet[i] + dslpt[i]

for i in range(0, id):
    daved[i] = daved[i] / itime
    davet[i] = davet[i] / itime

print('Darwin ave', daved[0], 'Tahiti ave', davet[0])

# variance, standard deviation, covariance
print('var, std')

for it in range(0, itime):
    dslpd = data_d[it, :]
    dslpt = data_t[it, :]
    for i in range(0, id):
        dvard[i] = dvard[i] + (dslpd[i] - daved[i])**2
        dvart[i] = dvart[i] + (dslpt[i] - davet[i])**2
        dcov[i] = dcov[i] + (dslpt[i] - davet[i]) * (dslpd[i] - daved[i])

for i in range(0, id):
    dvard[i] = dvard[i] / itime
    dstdd[i] = np.sqrt(dvard[i])
    dvart[i] = dvart[i] / itime
    dstdt[i] = np.sqrt(dvart[i])
    dcov[i] = dcov[i] / itime

print('Darwin', 'var', dvard[0], 'std', dstdd[0])
print('Tahiti', 'var', dvart[0], 'std', dstdt[0])
print('      ', 'cov', dcov[0])

#data_out = 'Darwin', 'var', dvard[0], 'std', dstdd[0]
#data_3.write(str(data_out))
#data_out = 'Tahiti', 'var', dvart[0], 'std', dstdt[0]
#data_3.write(str(data_out))
#data_out = '      ', 'cov', dcov[0]
#data_3.write(str(data_out))

# correlation

for i in range(0, id):
    dcor[i] = dcov[i] / (dstdd[i] * dstdt[i])

print('cor', dcor)
print('cov', dcov)
data_3.write('cor\n')
data_3.writelines(str(np.round(dcor,3)))
data_3.write('\ncov\n')
data_3.writelines(str(np.round(dcov,3)))
