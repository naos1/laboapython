#
# 21zonal-raw.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

data = data_in[1, :, :] # 1992 feb

for i in range(ilat):

     zave = 0
     for j in range(ilon):
          zave = zave + data[i, j]
     zave = zave / ilon

     lat = 90 - 2.5 * i
     print(lat, zave)
