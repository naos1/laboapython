#
# 19anomaly-func.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/uwnd.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

anom = data_in[71, :, :] - data_in[59, :, :] # 1997dec - 1996dec
