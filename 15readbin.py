#
# 15readbin.py
#
import numpy as np

itime, ilat, ilon = 120, 73, 144

f = open('data/slp.bin', 'rb')
data_in = np.fromfile(f, dtype='>f4')
data_in = data_in.reshape((itime, ilat, ilon))

data = data_in[0]

for i in range(ilat):
     for j in range(ilon):
          lon = 2.5 * j
          lat = 90 - 2.5 * i
          print(lon, lat, data[i, j])
